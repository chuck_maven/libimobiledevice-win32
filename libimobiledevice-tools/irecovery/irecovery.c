/*
* irecovery.c
* Software frontend for iBoot/iBSS communication with iOS devices
*
* Copyright (c) 2012-2013 Martin Szulecki <m.szulecki@libimobiledevice.org>
* Copyright (c) 2010-2011 Chronic-Dev Team
* Copyright (c) 2010-2011 Joshua Hill
* Copyright (c) 2008-2011 Nicolas Haunold
*
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the GNU Lesser General Public License
* (LGPL) version 2.1 which accompanies this distribution, and is available at
* http://www.gnu.org/licenses/lgpl-2.1.html
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libirecovery.h>

#ifdef WIN32
#include <windows.h>
#ifndef sleep
#define sleep(n) Sleep(1000 * n)
#endif
#define _FMT_lld "%I64d"
#else
#define _FMT_lld "%lld"
#endif

#define FILE_HISTORY_PATH ".irecovery"
#define debug(...) if(verbose) fprintf(stderr, __VA_ARGS__)

enum {
    kResetDevice,
    kSendCommand,
    kSendFile,
    kSendExploit,
    kSendScript,
    kShowMode,
    kRebootToNormalMode
};

static unsigned int quit = 0;
static unsigned int verbose = 0;

void print_progress_bar(double progress);
int received_cb(irecv_client_t client, const irecv_event_t* event);
int progress_cb(irecv_client_t client, const irecv_event_t* event);
int precommand_cb(irecv_client_t client, const irecv_event_t* event);
int postcommand_cb(irecv_client_t client, const irecv_event_t* event);

static const char* mode_to_str(int mode) {
    switch (mode) {
    case IRECV_K_RECOVERY_MODE_1:
    case IRECV_K_RECOVERY_MODE_2:
    case IRECV_K_RECOVERY_MODE_3:
    case IRECV_K_RECOVERY_MODE_4:
        return "Recovery";
        break;
    case IRECV_K_DFU_MODE:
        return "DFU";
        break;
    case IRECV_K_WTF_MODE:
        return "WTF";
        break;
    default:
        return "Unknown";
        break;
    }
}

static void buffer_read_from_filename(const char *filename, char **buffer, uint64_t *length) {
    FILE *f;
    uint64_t size;

    *length = 0;

    f = fopen(filename, "rb");
    if (!f) {
        return;
    }

    fseek(f, 0, SEEK_END);
    size = ftell(f);
    rewind(f);

    if (size == 0) {
        fclose(f);
        return;
    }

    *buffer = (char*)malloc(sizeof(char)*(size + 1));
    fread(*buffer, sizeof(char), size, f);
    fclose(f);

    *length = size;
}

int progress_cb(irecv_client_t client, const irecv_event_t* event) {
    if (event->type == IRECV_PROGRESS) {
        print_progress_bar(event->progress);
    }

    return 0;
}

void print_progress_bar(double progress) {
    int i = 0;

    if (progress < 0) {
        return;
    }

    if (progress > 100) {
        progress = 100;
    }

    printf("\r[");

    for (i = 0; i < 50; i++) {
        if (i < progress / 2) {
            printf("=");
        }
        else {
            printf(" ");
        }
    }

    printf("] %3.1f%%", progress);

    fflush(stdout);

    if (progress == 100) {
        printf("\n");
    }
}

static void print_usage(int argc, char **argv) {
    char *name = NULL;
    name = strrchr(argv[0], '/');
    printf("Usage: %s [OPTIONS]\n", (name ? name + 1 : argv[0]));
    printf("Interact with an iOS device in DFU or recovery mode.\n\n");
    printf("options:\n");
    printf("  -i ECID\tconnect to specific device by its hexadecimal ECID\n");
    printf("  -c CMD\trun CMD on device\n");
    printf("  -m\t\tprint current device mode\n");
    printf("  -f FILE\tsend file to device\n");
    printf("  -k FILE\tsend limera1n usb exploit payload from FILE\n");
    printf("  -r\t\treset client\n");
    printf("  -n\t\treboot device into normal mode (exit recovery loop)\n");
    printf("  -e FILE\texecutes recovery script from FILE\n");
    printf("  -v\t\tenable verbose output, repeat for higher verbosity\n");
    printf("  -h\t\tprints this usage information\n");
    printf("\n");
}

int main(int argc, char* argv[]) {
    int i = 0;
    int opt = 0;
    int action = 0;
    unsigned long long ecid = 0;
    int mode = -1;
    char* argument = NULL;
    irecv_error_t error = 0;

    char* buffer = NULL;
    uint64_t buffer_length = 0;

    if (argc == 1) {
        print_usage(argc, argv);
        return 0;
    }

    while ((opt = getopt(argc, argv, "i:vhrsmnc:f:e:k::")) > 0) {
        switch (opt) {
        case 'i':
            if (optarg) {
                char* tail = NULL;
                ecid = strtoull(optarg, &tail, 16);
                if (tail && (tail[0] != '\0')) {
                    ecid = 0;
                }
                if (ecid == 0) {
                    fprintf(stderr, "ERROR: Could not parse ECID from argument '%s'\n", optarg);
                    return -1;
                }
            }
            break;

        case 'v':
            verbose += 1;
            break;

        case 'h':
            print_usage(argc, argv);
            return 0;

        case 'm':
            action = kShowMode;
            break;

        case 'n':
            action = kRebootToNormalMode;
            break;

        case 'r':
            action = kResetDevice;
            break;

        case 'f':
            action = kSendFile;
            argument = optarg;
            break;

        case 'c':
            action = kSendCommand;
            argument = optarg;
            break;

        case 'k':
            action = kSendExploit;
            argument = optarg;
            break;

        case 'e':
            action = kSendScript;
            argument = optarg;
            break;

        default:
            fprintf(stderr, "Unknown argument\n");
            return -1;
        }
    }

    if (verbose)
        irecv_set_debug_level(verbose);

    irecv_init();
    irecv_client_t client = NULL;
    for (i = 0; i <= 5; i++) {
        debug("Attempting to connect... \n");

        if (irecv_open_with_ecid(&client, ecid) != IRECV_E_SUCCESS)
            sleep(1);
        else
            break;

        if (i == 5) {
            return -1;
        }
    }

    irecv_device_t device = NULL;
    irecv_devices_get_device_by_client(client, &device);
    if (device)
        debug("Connected to %s, model %s, cpid 0x%04x, bdid 0x%02x\n", device->product_type, device->hardware_model, device->chip_id, device->board_id);

    switch (action) {
    case kResetDevice:
        irecv_reset(client);
        break;

    case kSendFile:
        irecv_event_subscribe(client, IRECV_PROGRESS, &progress_cb, NULL);
        error = irecv_send_file(client, argument, 1);
        debug("%s\n", irecv_strerror(error));
        break;

    case kSendCommand:
        error = irecv_send_command(client, argument);
        debug("%s\n", irecv_strerror(error));
        break;

    case kSendExploit:
        if (argument != NULL) {
            irecv_event_subscribe(client, IRECV_PROGRESS, &progress_cb, NULL);
            error = irecv_send_file(client, argument, 0);
            if (error != IRECV_E_SUCCESS) {
                debug("%s\n", irecv_strerror(error));
                break;
            }
        }
        error = irecv_trigger_limera1n_exploit(client);
        debug("%s\n", irecv_strerror(error));
        break;

    case kSendScript:
        buffer_read_from_filename(argument, &buffer, &buffer_length);
        if (buffer) {
            buffer[buffer_length] = '\0';

            error = irecv_execute_script(client, buffer);
            if (error != IRECV_E_SUCCESS) {
                debug("%s\n", irecv_strerror(error));
            }

            free(buffer);
        }
        else {
            fprintf(stderr, "Could not read file '%s'\n", argument);
        }
        break;

    case kShowMode:
        irecv_get_mode(client, &mode);
        printf("%s Mode\n", mode_to_str(mode));
        break;

    case kRebootToNormalMode:
        error = irecv_setenv(client, "auto-boot", "true");
        if (error != IRECV_E_SUCCESS) {
            debug("%s\n", irecv_strerror(error));
            break;
        }

        error = irecv_saveenv(client);
        if (error != IRECV_E_SUCCESS) {
            debug("%s\n", irecv_strerror(error));
            break;
        }

        error = irecv_reboot(client);
        if (error != IRECV_E_SUCCESS) {
            debug("%s\n", irecv_strerror(error));
        }
        else {
            debug("%s\n", irecv_strerror(error));
        }
        break;
    default:
        fprintf(stderr, "Unknown action\n");
        break;
    }

    irecv_close(client);

    return 0;
}